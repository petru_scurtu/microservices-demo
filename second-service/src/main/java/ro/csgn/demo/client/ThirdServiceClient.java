package ro.csgn.demo.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "third-service")
public interface ThirdServiceClient {

    @GetMapping("/third-service/api/test")
    String testThirdService();
}
