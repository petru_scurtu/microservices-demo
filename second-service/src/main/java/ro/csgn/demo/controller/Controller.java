package ro.csgn.demo.controller;

import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.csgn.demo.client.FirstServiceClient;
import ro.csgn.demo.client.ThirdServiceClient;

@RestController
@RequestMapping("api")
public class Controller {

    private final Environment environment;
    private final FirstServiceClient firstServiceClient;
    private final ThirdServiceClient thirdServiceClient;

    public Controller(Environment environment, FirstServiceClient firstServiceClient, ThirdServiceClient thirdServiceClient) {
        this.environment = environment;
        this.firstServiceClient = firstServiceClient;
        this.thirdServiceClient = thirdServiceClient;
    }

    @GetMapping("/test")
    public String test(){
        return "Working on "+environment.getProperty("local.server.port");
    }

    @GetMapping("/call-first-service/test")
    public String firstServiceTest(){
        String s = "Called first service and received ";
        String reply = firstServiceClient.testFirstService();
        return s + reply;
    }

    @GetMapping("/call-third-service/test")
    public String thirdServiceTest(){
        String s = "Called third service and received ";
        String reply = thirdServiceClient.testThirdService();
        return s + reply;
    }
}
