package ro.csgn.demo.controller;

import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.csgn.demo.client.FirstServiceClient;
import ro.csgn.demo.client.SecondServiceClient;

@RestController
@RequestMapping("api")
public class Controller {

    private final Environment environment;
    private final FirstServiceClient firstServiceClient;
    private final SecondServiceClient secondServiceClient;

    public Controller(Environment environment, FirstServiceClient firstServiceClient, SecondServiceClient secondServiceClient) {
        this.environment = environment;
        this.firstServiceClient = firstServiceClient;
        this.secondServiceClient = secondServiceClient;
    }

    @GetMapping("/test")
    public String test() {
        return "Working on " + environment.getProperty("local.server.port");
    }

    @GetMapping("/call-first-service/test")
    public String firstServiceTest() {
        String s = "Called first service and received ";
        String reply = firstServiceClient.testFirstService();
        return s + reply;
    }

    @GetMapping("/call-second-service/test")
    public String secondServiceTest() {
        String s = "Called second service and received ";
        String reply = secondServiceClient.testSecondService();
        return s + reply;
    }

}
