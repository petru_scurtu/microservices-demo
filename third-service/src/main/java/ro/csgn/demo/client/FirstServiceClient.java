package ro.csgn.demo.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "first-service")
public interface FirstServiceClient {

    @GetMapping("/first-service/api/test")
    String testFirstService();
}
