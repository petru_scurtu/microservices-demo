package ro.csgn.demo.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "second-service")
public interface SecondServiceClient {

    @GetMapping("/second-service/api/test")
    String testSecondService();
}
