package ro.csgn.demo.controller;

import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.csgn.demo.client.SecondServiceClient;
import ro.csgn.demo.client.ThirdServiceClient;
import ro.csgn.demo.model.Person;
import ro.csgn.demo.service.PersonService;

import java.util.List;

@RestController
@RequestMapping("api")
public class Controller {

    private final Environment environment;
    private final SecondServiceClient secondServiceClient;
    private final ThirdServiceClient thirdServiceClient;
    private final PersonService personService;

    public Controller(Environment environment, SecondServiceClient secondServiceClient, ThirdServiceClient thirdServiceClient, PersonService personService) {
        this.environment = environment;
        this.secondServiceClient = secondServiceClient;
        this.thirdServiceClient = thirdServiceClient;
        this.personService = personService;
    }

    @GetMapping("/test")
    public String test(){
        return "Working on "+environment.getProperty("local.server.port");
    }

    @GetMapping("/call-second-service/test")
    public String secondServiceTest(){
        String s = "Called second service and received ";
        String reply = secondServiceClient.testSecondService();
        return s + reply;
    }

    @GetMapping("/call-third-service/test")
    public String thirdServiceTest(){
        String s = "Called third service and received ";
        String reply = thirdServiceClient.testThirdService();
        return s + reply;
    }

    @PostMapping("/addPerson")
    public Person add(){
        return personService.add();
    }

    @GetMapping("/listPersons")
    public List<Person> list(){
        return personService.list();
    }
}
