package ro.csgn.demo.service;

import org.springframework.stereotype.Repository;
import ro.csgn.demo.model.Person;
import ro.csgn.demo.repository.PersonRepository;

import java.util.List;

@Repository
public class PersonService {

    private final PersonRepository personRepository;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public Person add(){
        Person person = new Person();
        person.setFirstName("test");
        person.setLastName("test");
        person.setCareer("test");
        return personRepository.save(person);
    }

    public List<Person> list(){
        return personRepository.findAll();
    }
}
