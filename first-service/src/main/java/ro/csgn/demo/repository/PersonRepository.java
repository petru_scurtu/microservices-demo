package ro.csgn.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.csgn.demo.model.Person;

public interface PersonRepository extends JpaRepository<Person, Integer> {
}
